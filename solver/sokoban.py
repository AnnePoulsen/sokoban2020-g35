import sys
import collections
import numpy as np
import heapq
import time

class PriorityQueue:
    """Define a PriorityQueue data structure that will be used"""
    def  __init__(self):
        self.Heap = []
        self.Count = 0

    def push(self, item, priority):
        entry = (priority, self.Count, item)
        heapq.heappush(self.Heap, entry)
        self.Count += 1

    def pop(self):
        (_, _, item) = heapq.heappop(self.Heap)
        return item

    def isEmpty(self):
        return len(self.Heap) == 0

## Load puzzles and define the rules of sokoban - inspiration taken from ther code

def transferToGameState(layout):
    layout = [x.replace('\n','') for x in layout]
    layout = [','.join(layout[i]) for i in range(len(layout))]
    layout = [x.split(',') for x in layout]
    maxColsNum = max([len(x) for x in layout])
    for row in range(len(layout)):
        for col in range(len(layout[row])):
            if layout[row][col] == '.': layout[row][col] = 0   # paths
            elif layout[row][col] == 'X': layout[row][col] = 1 # walls
            elif layout[row][col] == 'M': layout[row][col] = 2 # initial position of robot
            elif layout[row][col] == 'J': layout[row][col] = 3 # initial position of cans
            elif layout[row][col] == 'G': layout[row][col] = 4 # goals
            elif layout[row][col] == 'S': layout[row][col] = 5 # cans on goal - as this is a possiblity.
        colsNum = len(layout[row])
        if colsNum < maxColsNum:
            layout[row].extend([1 for _ in range(maxColsNum-colsNum)]) 
    return np.array(layout)

def PlayerPos(gameState):
    ##Return the position of the robot agent
    return tuple(np.argwhere(gameState == 2)[0]) # e.g. (2, 2)

def CanPos(gameState):
    ##Return the positions of the cans
    return tuple(tuple(x) for x in np.argwhere((gameState == 3) | (gameState == 5))) # e.g. ((2, 3), (3, 4), (4, 4), (6, 1), (6, 4), (6, 5))

def WallPos(gameState):
    ##Return the positions of all the walls to avoid mistaking a wall for a passage
    return tuple(tuple(x) for x in np.argwhere(gameState == 1)) # e.g. like those above

    #Returns the positions of goals - stationary 
def GoalPos(gameState):
    return tuple(tuple(x) for x in np.argwhere((gameState == 4) | (gameState == 5))) # e.g. like those above

    #Check to see if goal-node has been found - eg if all cans are on a goal
def EndState(posCan):
    return sorted(posCan) == sorted(posGoals)

    # checks to see if the action is legal eg moves into a wall, pushijng a box into a wall etc. 
def isLegalAction(action, posPlayer, posCan):
    xPlayer, yPlayer = posPlayer
    if action[-1].isupper(): # the move was a push
        x1, y1 = xPlayer + 2 * action[0], yPlayer + 2 * action[1]
    else:
        x1, y1 = xPlayer + action[0], yPlayer + action[1]
    return (x1, y1) not in posCan + posWalls

def legalAction(posPlayer, posCan):
    ##Return all legal actions for the robot agent in the current game state compared to the world frame 
    allActions = [[-1,0,'u','U'],[1,0,'d','D'],[0,-1,'l','L'],[0,1,'r','R']]
    xPlayer, yPlayer = posPlayer
    legalActions = []
    for action in allActions:
        x1, y1 = xPlayer + action[0], yPlayer + action[1]
        if (x1, y1) in posCan: # the move was a push
            action.pop(2) # drop the little letter
        else:
            action.pop(3) # drop the upper letter
        if isLegalAction(action, posPlayer, posCan):
            legalActions.append(action)
        else: 
            continue     
    return tuple(tuple(x) for x in legalActions) # e.g. ((0, -1, 'l'), (0, 1, 'R'))

def updateState(posPlayer, posCan, action):
    ##Return updated game state after an action is taken 
    xPlayer, yPlayer = posPlayer # the previous position of player
    newPosPlayer = [xPlayer + action[0], yPlayer + action[1]] # the current position of player
    posCan = [list(x) for x in posCan]
    if action[-1].isupper(): # if pushing, update the position of can and return uppercase letter
        posCan.remove(newPosPlayer)
        posCan.append([xPlayer + 2 * action[0], yPlayer + 2 * action[1]])
    posCan = tuple(tuple(x) for x in posCan)
    newPosPlayer = tuple(newPosPlayer)
    return newPosPlayer, posCan

def isFailed(posCan):
    ## taken to get BFS to work 
    rotatePattern = [[0,1,2,3,4,5,6,7,8],
                    [2,5,8,1,4,7,0,3,6],
                    [0,1,2,3,4,5,6,7,8][::-1],
                    [2,5,8,1,4,7,0,3,6][::-1]]
    flipPattern = [[2,1,0,5,4,3,8,7,6],
                    [0,3,6,1,4,7,2,5,8],
                    [2,1,0,5,4,3,8,7,6][::-1],
                    [0,3,6,1,4,7,2,5,8][::-1]]
    allPattern = rotatePattern + flipPattern

    for box in posCan:
        if box not in posGoals:
            board = [(box[0] - 1, box[1] - 1), (box[0] - 1, box[1]), (box[0] - 1, box[1] + 1), 
                    (box[0], box[1] - 1), (box[0], box[1]), (box[0], box[1] + 1), 
                    (box[0] + 1, box[1] - 1), (box[0] + 1, box[1]), (box[0] + 1, box[1] + 1)]
            for pattern in allPattern:
                newBoard = [board[i] for i in pattern]
                if newBoard[1] in posWalls and newBoard[5] in posWalls: return True
                elif newBoard[1] in posCan and newBoard[2] in posWalls and newBoard[5] in posWalls: return True
                elif newBoard[1] in posCan and newBoard[2] in posWalls and newBoard[5] in posCan: return True
                elif newBoard[1] in posCan and newBoard[2] in posCan and newBoard[5] in posCan: return True
                elif newBoard[1] in posCan and newBoard[6] in posCan and newBoard[2] in posWalls and newBoard[3] in posWalls and newBoard[8] in posWalls: return True
    return False

# taken from https://github.com/KnightofLuna/sokoban-solver - see explnation in 
def breadthFirstSearch():
    beginBox = PosOfBoxes(gameState)
    beginPlayer = PosOfPlayer(gameState)

    startState = (beginPlayer, beginBox) # e.g. ((2, 2), ((2, 3), (3, 4), (4, 4), (6, 1), (6, 4), (6, 5)))
    frontier = collections.deque([[startState]]) # store states
    actions = collections.deque([[0]]) # store actions
    exploredSet = set()
    while frontier:
        node = frontier.popleft()
        node_action = actions.popleft() 
        if isEndState(node[-1][-1]):
            print(','.join(node_action[1:]).replace(',',''))
            break
        if node[-1] not in exploredSet:
            exploredSet.add(node[-1])
            for action in legalActions(node[-1][0], node[-1][1]):
                newPosPlayer, newposCan = updateState(node[-1][0], node[-1][1], action)
                if isFailed(newposCan):
                    continue
                frontier.append(node + [(newPosPlayer, newposCan)])
                actions.append(node_action + [action[-1]])


def heuristic(posPlayer, posCan):
    # using manhattan distance
    distance = 0
    completes = set(posGoals) & set(posCan)
    sortposCan = list(set(posCan).difference(completes))
    sortposGoals = list(set(posGoals).difference(completes))
    min_dist = 2**31
    for i in range(len(sortposCan)):
        new_dist =  (abs(sortposCan[i][0] - sortposGoals[i][0])) + (abs(sortposCan[i][1] - sortposGoals[i][1]))
        if(new_dist < min_dist):
            min_dist = new_dist
        distance += min_dist
    return distance

def cost(actions):
    ##A cost function - the most basic, just see how many steps has been taken as we consider every move to be of the same cost
    return len([x for x in actions if x.islower()])
"""If we were to implement going for straight routes more than turns, it would be benefitiel to change this cost function - a start to this can be seen underneath """
#def storaged(posCan, state, index):
 #   if (state.restrictions):
 #       return box in state.restriction[index]
 #   else:
 #       return box in state.storage
        
# implmentation of astar algiirthm using Manhattan distance as an admissible heurisitcs. 
def aStar():
    beginBox = PosOfBoxes(gameState)
    beginPlayer = PosOfPlayer(gameState)

    start_state = (beginPlayer, beginBox)
    openList = PriorityQueue()
    openList.push([start_state], heuristic(beginPlayer, beginBox))
    closedList = set()
    actions = PriorityQueue()
    actions.push([0], heuristic(beginPlayer, start_state[1])) # push first possible actions of state 0. 
    while openList:
        node = openList.pop()
        node_action = actions.pop()
        if isEndState(node[-1][-1]):
            print(','.join(node_action[1:]).replace(',',''))
            break
        if node[-1] not in closedList:
            closedList.add(node[-1]) # place the last node in closedLIst
            g = cost(node_action[1:]);
            for action in legalActions(node[-1][0], node[-1][1]):
                newPosPlayer, newposCan = updateState(node[-1][0], node[-1][1], action)
                if isFailed(newposCan):
                    continue 
                h = heuristic(newPosPlayer, newposCan)
                openList.push(node + [(newPosPlayer, newposCan)], h + g +1) 
                actions.push(node_action + [action[-1]], h + g+1)

    #help function to easily switch between levels and between the two diffirent solutions. Inspiration has been taken from https://github.com/KnightofLuna/sokoban-solver - adjusted to work inly with astar and bfs
def readCommand(argv):
    from optparse import OptionParser
    
    parser = OptionParser()
    parser.add_option('-l', '--level', dest='Levels',
                      help='level of game to play', default='level1.txt')
    parser.add_option('-m', '--method', dest='agentMethod',
                      help='research method', default='bfs')
    args = dict()
    options, _ = parser.parse_args(argv)
    with open('Levels/'+options.Levels,"r") as f: 
        layout = f.readlines()
    args['layout'] = layout
    args['method'] = options.agentMethod
    return args


if __name__ == '__main__':
    time_start = time.time()
    layout, method = readCommand(sys.argv[1:]).values()
    gameState = transferToGameState(layout)
    posWalls = PosOfWalls(gameState)
    posGoals = PosOfGoals(gameState)
    if method == 'a':
        aStar()
    elif method == 'bfs':
        breadthFirstSearch()
    else:
        raise ValueError('Invalid method.')
    time_end=time.time()
    print('Runtime of %s: %.2f second.' %(method, time_end-time_start))
