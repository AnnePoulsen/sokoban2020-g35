#!/usr/bin/env python3 

openSteps = open('steps.txt','r',);
steps = openSteps.read();
ang = 0;

def convertToWorld(step):
    global ang;
    if(ang == -90):
        ang = 270;
    if(ang == -180):
        ang = 180;
    if(ang == -270):
        ang = 90;
    if(ang == 360):
        ang = 0;
    if(ang == -360):
        ang = 0;
    if(ang == 450):
        ang = 90;

    if(ang == 0):
        if(step == 'u'):
            ang += 0;
            return 'f';
        if(step == 'U'):
            ang += 0;
            return 'F';
        if(step == 'r'):
            ang += 90;
            return 'r';
        if(step == 'R'):
            ang += 90;
            return 'R';
        if(step == 'l'):
            ang += -90;
            return 'l';
        if(step == 'L'):
            ang += -90;
            return 'L';
        if(step == 'd'):
            ang += 180;
            return 'b';
        if(step == 'D'):
            ang += 180;
            return 'B';
    if(ang == 90):
        if(step == 'u'):
            ang += -90;
            return 'l';
        if(step == 'U'):
            ang += -90;
            return 'L';
        if(step == 'r'):
            ang += 0;
            return 'f';
        if(step == 'R'):
            ang += 0;
            return 'F';
        if(step == 'l'):
            ang += 180;
            return 'b';
        if(step == 'L'):
            ang += 180;
            return 'B';
        if(step == 'd'):
            ang += 90;
            return 'r';
        if(step == 'D'):
            ang += 90;
            return 'R';
    if(ang == 180):
        if(step == 'u'):
            ang += 180;
            return 'b';
        if(step == 'U'):
            ang += 180;
            return 'B';
        if(step == 'r'):
            ang += -90;
            return 'l';
        if(step == 'R'):
            ang += -90;
            return 'L';
        if(step == 'l'):
            ang += 90;
            return 'r';
        if(step == 'L'):
            ang += 90;
            return 'R';
        if(step == 'd'):
            ang += 0;
            return 'f';
        if(step == 'D'):
            ang += 0;
            return 'F';
    if(ang == 270):
        if(step == 'u'):
            ang += 90;
            return 'r';
        if(step == 'U'):
            ang += 90;
            return 'R';
        if(step == 'r'):
            ang += 180;
            return 'b';
        if(step == 'R'):
            ang += 180;
            return 'B';
        if(step == 'l'):
            ang += 0;
            return 'f';
        if(step == 'L'):
            ang += 0;
            return 'F';
        if(step == 'd'):
            ang += -90;
            return 'l';
        if(step == 'D'):
            ang += -90;
            return 'L';


stepsConverted = [];
for i in range(0,len(steps)-1):
	stepsConverted.append(convertToWorld(steps[i]));
stepsConverted.append('o');
openSteps.close();

convertedSteps = open('stepsConverted.txt','w',);
for i in range(0,len(stepsConverted)) :
    convertedSteps.write(stepsConverted[i]) ;
    convertedSteps.write(',');

convertedSteps.close();
