#!/usr/bin/env pybricks-micropython
# https://pybricks.github.io/ev3-micropython/ev3devices.html
"""
Example LEGO® MINDSTORMS® EV3 Robot Educator Color Sensor Down Program
----------------------------------------------------------------------

This program requires LEGO® EV3 MicroPython v2.0.
Download: https://education.lego.com/en-us/support/mindstorms-ev3/python-for-ev3

Building instringeructions can be found at:
https://education.lego.com/en-us/support/mindstorms-ev3/building-instringeructions#robot
"""
from pybricks.hubs import EV3Brick
from pybricks.ev3devices import Motor, ColorSensor
from pybricks.parameters import Port
from pybricks.tools import wait
from pybricks.robotics import DriveBase

# instringeruction map
file1 = open("stepsConverted.txt","r")
for line in file1:
    csv = line.split(",")
# Initialize the motors.
left_motor = Motor(Port.B)
right_motor = Motor(Port.C)

# Initialize the color sensor.
line_sensor_left = ColorSensor(Port.S3)
line_sensor_right = ColorSensor(Port.S4)

# Initialize the drive base.
robot = DriveBase(left_motor, right_motor, wheel_diameter=55.5, axle_track=104)

# Calculate the light threshold. Choose values based on your measurements.
BLACK = 0
WHITE = 70
threshold = 20 
threshold_larger = (WHITE + BLACK)/2
threhold_lower = 15
# Set the drive speed at 100 millimeters per second.
DRIVE_SPEED = 100
NoM = 0
i = 0 

# Set the gain of the proportional line controller. This means that for every
# percentage point of light deviating from the threshold, we set the turn
# rate of the drivebase to 1.2 degrees per second.

# For example, if the light value deviates from the threshold by 10, the robot
# steers at 10*1.2 = 12 degrees per second.
pg_n = 1.2 # when the deviation from line is less than threshold
pg_t = 2  # when the deviation from line is over threshold. 

left_arr = []
right_arr = []
count = 0
old_avg_r = 0
old_avg_l = 0   
new_avg_r = 0
new_avg_l = 0


def turnRight ():
    robot.reset()
    robot.drive(20, 0)
    while(robot.distance() < 40):
        pass
    robot.drive(0,0)
    wait(10)
    robot.drive(0, -90)
    while (robot.angle() > -90):
        pass
    robot.reset()
    forward()

def turnLeft ():
    robot.reset()
    robot.drive(20,0)
    while(robot.distance() < 40):
        pass
    robot.drive(0, 90)
    while (robot.angle() < 90):
        pass
    robot.reset()
    forward()

def backward():
    robot.reset()
    robot.drive(0, 180)
    while(robot.angle() < 180):
        pass
    wait(30)
    while (crossroad() == 0 ):
        robot.drive(DRIVE_SPEED, onLine())
        wait(10)
    robot.reset()

def forward():
    robot.reset()
    robot.drive(DRIVE_SPEED, 0)
    wait(20)
    while (robot.distance() < 200 ): 
        robot.drive(DRIVE_SPEED, onLine())
        wait(10)
    while(crossroad() == 0):
        robot.drive(DRIVE_SPEED,onLine())    
    robot.reset()

#Used when the robot moves forward to place a can as it needs to stop before it detects a crossraod
def sForward():
    robot.reset()
    robot.drive(DRIVE_SPEED,0)
    wait(20)
    while(robot.distance() < 290):
        robot.drive(DRIVE_SPEED,0)
    robot.reset()
#Used to move backwards when placing a can, as a normal 180 turn would reorientate the agent and its displacement angle in reference to world frame would be 180 degrees off. 
def sBackward():
    robot.reset()
    robot.drive(-DRIVE_SPEED,0)
    wait(100)
    while(robot.distance() > -290):
        robot.drive(-DRIVE_SPEED, 0)
    while(crossroad() == 0):
        pass
    robot.reset()

def placeCan():
    sForward()
    wait(10)
    sBackward()

def direction(stringer):
    nextM  = csv[NoM+1]
    if(stringer == "R" or stringer == "r"):
        if(stringer == "R" and (nextM != "R" and nextM != "F" and nextM != "B" and nextM != "L")):
            turnRight()
            wait(5)
            forward()
            placeCan()
        else:
            turnRight()
    elif(stringer == "L" or stringer=="l"):
        if(stringer == "L"and (nextM != "L" and nextM != "F" and nextM != "B" and nextM != "R")):
            turnLeft()
            wait(5)
            forward()
            placeCan()
        else:
            turnLeft()
    elif(stringer == "b" or stringer == "B"):
        if(stringer == "B"and (nextM != "F" and nextM != "B" and nextM != "L" and nextM != "R")):
            backward()
            placeCan()
        else:
            backward()
    elif(stringer == "F" or stringer == "f"):
        if(stringer == "F" and ((nextM != "F") and nextM != "L" and nextM != "R" and nextM != "B")):
            forward()
            placeCan()
        else:
            forward()
    elif(stringer == "o"):
        robot.drive(0,0)
        while(robot.distance() < 200):
            print("I am done!")
    else:
        robot.drive(0,0)

def crossroad():
    left_sensor = line_sensor_left.reflection()
    right_sensor = line_sensor_right.reflection()
    if(left_sensor < 15 and right_sensor < 15):
        i+1
        return(1)
    else:
        return(0)


def onLine():
    left_sensor = line_sensor_left.reflection()
    right_sensor = line_sensor_right.reflection()
    if(left_sensor < 15):
        deviation = left_sensor + threshold
        turn_rate = (-1)*pg_n*deviation
        return(turn_rate)
    elif(right_sensor < 15):
        deviation = right_sensor + threshold
        turn_rate = pg_n*deviation
        return(turn_rate)
    else:
        return(0)

def nextmove():
    return csv[NoM]

move = nextmove()

while True:
    if(crossroad() == 1):
        NoM = NoM + 1
        direction(nextmove())
        wait(10)
        move = nextmove()
    else:
        direction(move)
